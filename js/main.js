const block = document.getElementsByClassName("area");
const startBtn = document.getElementById("start-btn");
startBtn.hidden=true;

for(let i = 0;i<block.length;i++){
    let m = Math.round(Math.random()*2);
    if(m<1){
        block[i].classList.add("marked");
    }
};

for(let l=0;l<=56;l+=8){
    if(!block[l].classList.contains("marked")){
        block[l].classList.add("left");   
    }};

for(let r=7;r<=64;r+=8){
    if(!block[r].classList.contains("marked")){
     block[r].classList.add("right");   
    }};

for(let k=0;k<=block.length-1;k++){
    if(!block[k].classList.contains("marked")){
        block[k].innerHTML = 0;
        if(k>0 && block[k-1].classList.contains("marked") && !block[k].classList.contains("left")){
            block[k].innerHTML++;};
        if(k<63 && block[k+1].classList.contains("marked") && !block[k].classList.contains("right")){
            block[k].innerHTML++;};
        if(k<64 && k>0 && k<56 && block[k+7].classList.contains("marked") && !block[k].classList.contains("left")){
            block[k].innerHTML++;};
        if(k<64 && k<55 && block[k+8].classList.contains("marked")){
            block[k].innerHTML++;};
        if(k<64 && k<55 && block[k+9].classList.contains("marked") && !block[k].classList.contains("right")){
            block[k].innerHTML++;};
        if(k>0 && k>7 && block[k-7].classList.contains("marked") && !block[k].classList.contains("right")){
            block[k].innerHTML++;};
        if(k>0 && k>8 && block[k-8].classList.contains("marked")){
            block[k].innerHTML++;};
        if(k>0 && k>8 && block[k-9].classList.contains("marked") && !block[k].classList.contains("left")){
            block[k].innerHTML++;};
        }
    };

document.addEventListener("click",function(e){
    if(e.target.classList.contains("area")){
        startBtn.hidden = false;
    }
    if(e.target.className==="area marked"){
        const marked = document.getElementsByClassName("marked");
        for(let j =0;j<marked.length;j++){
            marked[j].classList.add("mine");
        }
        for(let e=0;e<block.length;e++){
            let flag=0;
            if(!block[e].classList.contains("mine")){
                block[e].classList.add("revealed");
                block[e].classList.remove("flag");
            }
        }
    }if(e.target.className!="area marked" && !e.target.classList.contains("flag")){
        e.target.classList.add("revealed");
        if(e.target.innerText == "0"){
            findZero = () => {
                for(let k=0;k<=block.length-1;k++){
                if(block[k] === e.target){
                    if(k>0 && !block[k].classList.contains("left")){
                        block[k-1].classList.add("revealed")};
                    if(k<63 && !block[k].classList.contains("right")){
                        block[k+1].classList.add("revealed")};
                    if(k<64 && k>0 && k<56 && !block[k].classList.contains("left")){
                        block[k+7].classList.add("revealed")};
                    if(k<64 && k<55){
                        block[k+8].classList.add("revealed")};
                    if(k<64 && k<55 && !block[k].classList.contains("right")){
                        block[k+9].classList.add("revealed")};
                    if(k>0 && k>7 && !block[k].classList.contains("right")){
                        block[k-7].classList.add("revealed")};
                    if(k>0 && k>8){
                        block[k-8].classList.add("revealed")};
                    if(k>0 && k>8 && !block[k].classList.contains("left")){
                        block[k-9].classList.add("revealed")};
                    }}
            };findZero();
        }
    }if(e.target.tagName === "BUTTON"){
        window.location.reload();
    }
});

let flag = 0;
const mines=document.getElementsByClassName("marked").length;
const counter = document.getElementById("counter");
counter.innerHTML=mines+" mines/"+"0 flags"

const table = document.querySelector(".table"); 
table.addEventListener("contextmenu",function(e){
    if(e.target.classList.contains("area")){
        startBtn.hidden = false;
    }
    e.preventDefault();
    if(!e.target.classList.contains("revealed")){
        e.target.classList.toggle("flag");
    }
    flag = document.getElementsByClassName("flag").length;
    return counter.innerHTML=mines+" mines/ "+flag+" flags";
    
});